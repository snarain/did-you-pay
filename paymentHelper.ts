
import path from 'path';
const fsPaymentsJSON = path.join(__dirname,'..','data/payments.json');
import fs from 'fs';
import Util from './Util';

export default class paymentHelper {


  static getAllPayments() {
    return JSON.parse(fs.readFileSync(fsPaymentsJSON,'utf-8'));
  }

  static getPaymentById(id: string): any {
    const payments = this.getAllPayments().filter( (p: any) => {
      return p.id === parseInt(id, 10);
    })
    return payments[0];
  }

  static filterPaymentsByCategory(categoryId: string) {
    console.log('Get payments with  category id ' + categoryId);
    let pymts = this.getAllPayments();
      
    return pymts.filter( (p: any) => {
        return p.category === categoryId;
    })
  }


  static savePayments(contents: any) {
    fs.writeFileSync(fsPaymentsJSON, JSON.stringify(contents));
  }


  /**
   * Get most recent id for project from data.json
   * @param tests 
   */
  static getRecentIdForPayment (payments: any) {

    console.log('Find recent id for payment ')
    let ids = payments.map( (p: any) => {
      return p.id;
    });
  
    console.log(ids);
  
    if(ids.length > 0) {
      return Math.max(...ids);
    }
    return -1;
  }

  static getStatusesForPayment(pId: string): string[] {
    console.log('Get statuses for payment: ', pId);
    const pymt = this.getPaymentById(pId);
    console.log(pymt);
    let statusList = [];
    statusList.push('PENDING#PENDING#' + (pymt.status === 'PENDING'));
    statusList.push('PAID#PAID#' + (pymt.status === 'PAID'));
    console.log(statusList);
    return statusList;
  }

  static setPaid(pId: string) {
    console.log('Settings payment of id ' + pId + ' to Paid');

    let pymts = paymentHelper.getAllPayments();
    for(let p of pymts) {
      if(p.id === parseInt(pId, 10)) {
        console.log('Setting status to Paid ');
        /**
         * Set previous to last paid with value of last paid
         * Set lastPaid with today's date
         * Offset recurring date 30 days after
         */
        p.previousToLastPaid = p.lastPaid;
        p.lastPaid = Util.convertToMMDDYY(new Date());
        p.recurDate = Util.getNextRecurringDate(p.recurDate);
        p.isNew = 0;
        break;
      }
    }
    paymentHelper.savePayments(pymts);
    return paymentHelper.getAllPayments();
  }

  static setPending(pId: string) {
   
    console.log('Settings payment of id ' + pId + ' to Pending ');

    let pymts = paymentHelper.getAllPayments();
    for(let p of pymts) {
      if(p.id === parseInt(pId, 10)) {
        console.log('Setting status to Pending ');
        p.lastPaid = p.previousToLastPaid;
        p.recurDate = Util.getPreviousRecurringDate(p.recurDate);
        break;
      }
    }
    paymentHelper.savePayments(pymts);
    return paymentHelper.getAllPayments();  
  }



  

}