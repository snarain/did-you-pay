const { series } = require('gulp');
const { src, dest } = require('gulp');

function copy() {
  return src('client/**/*', {"base": "."})
    .pipe(dest('dist/'));
}

exports.build = series(copy);