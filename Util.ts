export default class Util {

    public static convertToMMDDYY(d: any) {
        return d.getMonth() + 1  + '/' + d.getDate() + '/' + d.getFullYear();
    }

    /**
     * Offset month + 1
     * @param dateInput 
     */
  static getNextRecurringDate(dateInput: string) {

    let d = new Date(dateInput);
    let nexMonth;
    let nextYear = d.getFullYear();
    let incrementYear = false;

    if(d.getMonth() <= 10) {
        console.log('Increment month')
        nexMonth = d.getMonth() + 2;
    }
    else {
        console.log('set month to january')
        nexMonth = 1;
        incrementYear = true;
    }

    if(incrementYear === true) {
        console.log('increment year')
        nextYear = d.getFullYear() + 1;
    }
    return nexMonth + '/' +  d.getDate() + '/' + nextYear ;
  }


  /**
   * Offset month to -1
   * @param dateInput 
   */
  static getPreviousRecurringDate(dateInput: string) {
    let d = new Date(dateInput);
    let prevMonth;
    let prevYear = d.getFullYear();
    let decrementYear = false;

    if(d.getMonth() >= 2) {
        console.log('Decrement month')
        prevMonth = d.getMonth();
    }
    else {
        console.log('set month to dec')
        prevMonth = 11;
        decrementYear = true;
    }

    if(decrementYear === true) {
        console.log('decrement year')
        prevYear = d.getFullYear() - 1;
    }
    return prevMonth + '/' +  d.getDate() + '/' + prevYear ;

  }

  static isLessThanTodaysDate(dateInput: string) {
    let today = new Date();
    return new Date(dateInput) < today;
  }


}