export default class Env {


     static getHost() {
        return process.env.DYP_HOST_NAME || 'localhost';
    }

     static getPort() {
        return process.env.DYP_PORT_NUM || '8080';
    }

}