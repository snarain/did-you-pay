var express = require('express');
var app = express();
const bodyParser = require('body-parser');
var path = require('path')
import paymentHelper from './paymentHelper';
import Util from './Util';
import Env from './Env';


app.engine('html', require('ejs').renderFile); 
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(express.static("./css"));


app.get('/', function(req: any, res: any) {
    res.redirect('/home');
})


app.get('/addNewPayment*', function(req: any, res: any) {
  res.render(__dirname+ '/client/addNewPayment.html', {error: undefined, env: Env});
})


app.post('/addNewPayment*', function(req: any, res: any) {
  const payments = paymentHelper.getAllPayments();

  console.log(req.body);

  if(req.body.clear) {
    console.log('Navigate home');
    res.render(__dirname+ '/client/home.html', {payments: undefined, error: undefined, env: Env});
    return;
  }
  
  if(req.body.name.trim().length > 0 && 
        req.body.desc.trim().length >0 && req.body.recurDate.trim().length > 0) {
  
    console.log('Add new payment')

    if(Util.isLessThanTodaysDate(req.body.recurDate)) {
      res.render(__dirname+ '/client/addNewPayment.html', { error: "Recurring date cannot be in the past.", env: Env});
      return;
    }

    let recentId = paymentHelper.getRecentIdForPayment(payments);

    let paymentId = (recentId === -1) ? 1 : recentId + 1;  
    console.log(paymentId);

    let currDate = new Date();
    let currDateStr = currDate.getMonth() +1 + '/' + currDate.getDate() + '/' + currDate.getFullYear();
  
    payments.push( { id: paymentId,
                      name: req.body.name,
                      description: req.body.desc,
                      recurDate: req.body.recurDate,
                      lastPaid: currDateStr,
                      previousToLastPaid: currDateStr, // payment made previous to last payment,
                      isNew : 1
                    });
  
    paymentHelper.savePayments(payments);
    res.render(__dirname+ '/client/addNewPayment.html', {error: "Payment successfully added", env: Env});

  }
  else {
    res.render(__dirname+ '/client/addNewPayment.html', { error: "One or more fields is empty.", env: Env});
  }

})

 


app.get('/home.html', function(req: any, res: any) {
  res.redirect('/home');
})

app.get('/home', function(req: any, res: any) {
  console.log('Home:')

  const payments = paymentHelper.getAllPayments();

  console.log(req.query)

    res.render(__dirname+ '/client/home.html', {payments: payments, error: undefined, env: Env});

})


app.get('/deletePayment*', function(req:any, res: any) {

    let pymts = paymentHelper.getAllPayments();

    if(pymts.length > 0) {
        res.render(__dirname+ '/client/deletePayment.html', {payments: pymts, error: undefined, env: Env});
    }
    else {
        res.render(__dirname+ '/client/deletePayment.html', {payments: pymts, error: 'There are no payments available', env: Env});
    }
})


app.post('/deletePayment*', function(req: any, res: any) {

  let pymts = paymentHelper.getAllPayments();

  let idToDel = req.body.paymentId;
  console.log('Delete payment with id: ', idToDel )

  if(idToDel) {
    let paymentsAfterDelete;
    
    if(Array.isArray(idToDel)) {
        console.log('Multi delete.', idToDel);

      // when ids are more than 1, it is sent as string array. 
      let idsToDel = idToDel.map( id => {
          return parseInt(id, 10);
      })
    
      paymentsAfterDelete = pymts.filter( (p: any) => {
        return !idsToDel.includes(p.id);
      })

    }
    else {

      idToDel = parseInt(idToDel, 10);
        console.log('Single test delete.', idToDel);

        paymentsAfterDelete = pymts.filter( (t: any) => {
            return idToDel !== t.id;
        })
    }
    console.log(paymentsAfterDelete);

  paymentHelper.savePayments(paymentsAfterDelete);

  res.render(__dirname+ '/client/deletePayment.html', {payments: paymentHelper.getAllPayments(), error: undefined, env: Env});
  }
  else {
    res.render(__dirname+ '/client/deletePayment.html', {payments: paymentHelper.getAllPayments(),
        error: "You must select atleast one payment to delete", env: Env});

  }


})

app.get('/editPayment*', function(req: any, res: any) {
    let pymtId = req.query.paymentId;
    console.log('Edit test with id ', pymtId)
    const pymts = paymentHelper.getAllPayments();
    
    const currPymt = paymentHelper.getPaymentById(pymtId);
    
    if(currPymt) {
      console.log(currPymt.name);

      let pymtStatuses : string[] = paymentHelper.getStatusesForPayment(pymtId);
      res.render(__dirname + '/client/editPayment.html', {currPayment : currPymt, 
          statuses: pymtStatuses, error: undefined, env: Env});
  }
  else {
      res.render(__dirname+ '/client/home.html', {error: 'You must select a payment to edit.', env: Env});
  }


})

app.post('/editPayment*', function(req: any, res: any) {

  if(req.body.cancel) {
    res.render(__dirname+ '/client/home.html', { payments: paymentHelper.getAllPayments(), error : undefined, env: Env});
  }

  if(req.body.save) {
    let pymtId = req.body.paymentId;
    console.log('update payment with id', pymtId);

    const pymts = paymentHelper.getAllPayments();
        
    for(let p of pymts) {
      if(p.id === parseInt(req.body.paymentId,10)) {
      console.log('update payment');
    
      p.name = req.body.name;
      p.description = req.body.desc;
      p.status = req.body.status;
      }
    }

    let currPymt = paymentHelper.getPaymentById(pymtId);

    if(req.body.name.trim().length == 0) {
      res.render(__dirname + '/client/editPayment.html', {currPayment : currPymt, error: "Name cannot be null", env: Env});
    }
    else if(req.body.desc.trim().length == 0) {
      res.render(__dirname + '/client/editPayment.html', {currPayment : currPymt, 
        error: "Description cannot be null. Its useful to add a description for easy reference.", env: Env});
    }
    else if(req.body.status.trim().length == 0) {
      res.render(__dirname + '/client/editPayment.html', {currPayment : currPymt, 
          error: "Status cannot be null", env: Env});
    }
    else {

      paymentHelper.savePayments(pymts);
      // refresh the test objects and projects references after update success.
      currPymt = paymentHelper.getPaymentById(pymtId);
      let statuses : string[] = paymentHelper.getStatusesForPayment(pymtId);
      res.render(__dirname + '/client/editPayment.html', {currPayment : currPymt, statuses: statuses, 
          error: "Update success", env: Env});

    }
  }
})

app.post('/updatePayment', function(req: any, res: any) {

  let paidAction = req.body.paid;
  let pendingAction = req.body.pending;
// update

if( paidAction || pendingAction) {

  console.log('Update status:', req.body);

  let id = req.body.id;

  let pymts = (paidAction) ? paymentHelper.setPaid(id) : paymentHelper.setPending(id);

  pymts = paymentHelper.getAllPayments();
  res.render(__dirname+ '/client/home.html', {payments: pymts, error: undefined, env: Env});

}

})


app.listen(8080);